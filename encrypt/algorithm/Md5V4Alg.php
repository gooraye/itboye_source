<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2016-11-15
 * Time: 9:45
 */

namespace app\src\encrypt\algorithm;

use app\src\encrypt\des\Des;
use app\src\encrypt\exception\CryptException;

/**
 * Class Md5V4Alg
 * 不进行任何操作
 * @author hebidu <email:346551990@qq.com>
 * @package app\src\encrypt\algorithm
 */
class Md5V4Alg extends IAlgorithm
{

    function decryptTransmissionData($transmissionData,$desKey)
    {
        return $transmissionData;
    }

    function encryptTransmissionData($param,$desKey)
    {
        return $data;
    }

    function verify_sign($sign,AlgParams $algParams)
    {
        return true;
    }

    function sign(AlgParams $param)
    {
        $param->isValid();

        return $param->getSign();
    }

    function decryptData($encryptData)
    {
        return json_decode($encryptData,JSON_OBJECT_AS_ARRAY);
    }

    function encryptData($data)
    {
        $str = json_encode($data,0, 512);
        return $str;
    }

}