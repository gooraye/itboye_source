<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2017-06-29
 * Time: 13:57
 */

namespace app\src\user\action;


use app\src\base\action\BaseAction;
use app\src\base\helper\ValidateHelper;
use app\src\user\logic\MemberConfigLogic;
use app\src\user\logic\UcenterMemberLogic;

class UserHelperAction extends BaseAction
{
    /**
     * 判断是否存在该uid
     * @param int $uid
     * @return bool true 存在 | false 不存在
     */
    public function existUid($uid=0){
        $result = (new UcenterMemberLogic())->getInfo(['id'=>$uid]);
        if(ValidateHelper::legalArrayResult($result) && $uid == $result['info']['id']){
            return true;
        }
        return false;
    }

    /**
     * 判断是否存在用户名
     * @param $username
     * @return bool true 存在 | false 不存在
     */
    public function existUsername($username){

        $result = (new UcenterMemberLogic())->getInfo(['username'=>$username]);
        if(ValidateHelper::legalArrayResult($result) && $username == $result['info']['username']){
            return true;
        }
        return false;
    }

    /**
     * 判断是否存在已验证的手机号
     * @param string $mobile  手机号
     * @param string $country_prefix 国家号码前缀
     * @return bool true 存在 | false 不存在
     */
    public function existValidPhone($mobile,$country_prefix='+86'){

        $result = (new UcenterMemberLogic())->getInfo(['mobile'=>$mobile,'country_no'=>$country_prefix]);
        if(ValidateHelper::legalArrayResult($result) && $mobile == $result['info']['mobile']){
            $uid = $result['info']['id'];
            $result = (new MemberConfigLogic())->getInfo(['uid'=>$uid]);
            if(ValidateHelper::legalArrayResult($result)){
                $phone_validate = $result['info']['phone_validate'];
                return $phone_validate == 1;
            }
        }
        return false;
    }

    /**
     * 判断是否存在已验证的邮箱
     * @param $email
     * @return bool true 存在 | false 不存在
     */
    public function existValidEmail($email){

        $result = (new UcenterMemberLogic())->getInfo(['email'=>$email]);
        if(ValidateHelper::legalArrayResult($result) && $email == $result['info']['email']){
            $uid = $result['info']['id'];
            $result = (new MemberConfigLogic())->getInfo(['uid'=>$uid]);
            if(ValidateHelper::legalArrayResult($result)){
                $email_validate = $result['info']['email_validate'];
                return $email_validate == 1;
            }
        }
        return false;
    }

}