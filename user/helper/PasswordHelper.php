<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2017-06-29
 * Time: 14:03
 */

namespace app\src\user\helper;


/**
 * 用户密码不可逆加密
 * Class PasswordHelper
 * @package app\src\user\helper
 */
class PasswordHelper
{
    public static function encrypt($str,$key='itboye'){
        return '' === $str ? '' : md5(sha1($str) . $key);
    }
}