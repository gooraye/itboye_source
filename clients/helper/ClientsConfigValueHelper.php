<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2017-06-30
 * Time: 11:13
 */

namespace app\src\clients\helper;


class ClientsConfigValueHelper
{
    /**
     *
     * @param $type
     * @param $value
     * @return array
     */
    public static function parse($type, $value) {
        switch ($type) {
            case 1 : // 字符串
                return $value;
            case 2 : // json字符串
                return json_decode($value);
            case 3 : // 数组
                $array = preg_split('/[,;\r\n]+/', trim($value, ",;\r\n"));
                if (strpos($value, ':')) {
                    $value = array();
                    foreach ($array as $val) {
                        list($k, $v) = explode(':', $val,2);
                        $value[$k] = $v;
                    }
                } else {
                    $value = $array;
                }
                break;
        }
        return $value;
    }
}