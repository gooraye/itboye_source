<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2017-06-26
 * Time: 16:14
 */

namespace app\src\clients\helper;

/**
 * 随机生成
 * Class RandomHelper
 * @package app\src\clients\helper
 */
class RandomHelper
{
    public static function getAppID($uid=0){
        return 'by'.dechex($uid).substr(md5(uniqid()),8,16);
    }

    public static function getAppSecret($uid=0){
        return dechex($uid).md5(uniqid()).dechex($uid);
    }
}