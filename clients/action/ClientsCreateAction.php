<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2017-06-26
 * Time: 16:12
 */

namespace app\src\clients\action;


use app\src\base\action\BaseAction;
use app\src\clients\helper\RandomHelper;
use app\src\clients\logic\ClientsLogic;

class ClientsCreateAction extends BaseAction
{
    public function create($uid,$name,$note=''){
        $app_id = RandomHelper::getAppID($uid);
        $app_secret = RandomHelper::getAppSecret($uid);
        $time = time();
        $entity = [
            'uid'=>$uid,
            'app_name'=>$name,
            'app_id'=>$app_id,
            'app_secret'=>$app_secret,
            'note'=>$note,
            'create_time'=>$time,
            'update_time'=>$time
        ];
        return (new ClientsLogic())->add($entity);
    }
}