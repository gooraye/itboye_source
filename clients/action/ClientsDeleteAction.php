<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2017-06-26
 * Time: 16:12
 */

namespace app\src\clients\action;


use app\src\base\action\BaseAction;
use app\src\base\utils\CodeGenerateUtils;
use app\src\clients\helper\RandomHelper;
use app\src\clients\logic\ClientsLogic;

class ClientsDeleteAction extends BaseAction
{
    public function deleteByID($id,$uid){
        $map = [
            'id'=>$id,
            'uid'=>$uid
        ];
        return (new ClientsLogic())->delete($map);
    }
    public function deleteByAppID($app_id,$uid){
        $map = [
            'app_id'=>$app_id,
            'uid'=>$uid
        ];
        return (new ClientsLogic())->delete($map);
    }
}