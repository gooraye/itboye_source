<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2017-06-30
 * Time: 10:59
 */

namespace app\src\clients\action;


use app\src\base\action\BaseAction;
use app\src\base\helper\PageHelper;
use app\src\clients\logic\ClientsConfigLogic;

/**
 * 应用配置创建
 * Class ClientsConfigQueryAction
 * @package app\src\clients\action
 */
class ClientsConfigQueryAction extends BaseAction
{
    public function query($app_id,$module_code='',PageHelper $pageHelper){
        $map = [
            'app_id'=>$app_id
        ];
        if(!empty($module_code)){
            $map['module_code'] = $module_code;
        }
        $result = (new ClientsConfigLogic())->query($map,$pageHelper->queryParam(),"create_time desc");
        return $result;
    }
}