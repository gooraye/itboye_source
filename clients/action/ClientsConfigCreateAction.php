<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2017-06-30
 * Time: 10:59
 */

namespace app\src\clients\action;


use app\src\base\action\BaseAction;
use app\src\clients\logic\ClientsConfigLogic;

/**
 * 应用配置创建
 * Class ClientsConfigCreateAction
 * @package app\src\clients\action
 */
class ClientsConfigCreateAction extends BaseAction
{
    public function create($entity){
        return (new ClientsConfigLogic())->add($entity);
    }
}