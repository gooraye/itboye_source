<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2017-06-30
 * Time: 10:59
 */

namespace app\src\clients\action;


use app\src\base\action\BaseAction;
use app\src\base\helper\PageHelper;
use app\src\clients\logic\ClientsConfigLogic;

/**
 * 应用配置创建
 * Class ClientsConfigDetailAction
 * @package app\src\clients\action
 */
class ClientsConfigDetailAction extends BaseAction
{
    public function detail($app_id,$module_code,$name){
        $map = [
            'app_id'=>$app_id,
            'module_code'=>$module_code,
            'name'=>$name
        ];
        $result = (new ClientsConfigLogic())->getInfo($map);
        return $result;
    }
}