<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2017-06-26
 * Time: 16:12
 */

namespace app\src\clients\action;


use app\src\base\action\BaseAction;
use app\src\base\helper\PageHelper;
use app\src\base\helper\ResultHelper;
use app\src\base\utils\CodeGenerateUtils;
use app\src\clients\helper\RandomHelper;
use app\src\clients\logic\ClientsLogic;

class ClientsUpdateAction extends BaseAction
{
    public function updateByID($id,$name,$note=''){
        $map = [
            'id'=>$id
        ];
        $update = [
        ];
        if(!empty($name)){
            $update['app_name'] = $name;
        }
        if(!empty($note)){
            $update['note'] = $note;
        }
        if(count($update) == 0){
            return ResultHelper::success('操作成功');
        }
        $update['update_time'] = time();
        return (new ClientsLogic())->save($map,$update);
    }

    public function updateByAppID($app_id,$name,$note=''){
        $map = [
            'app_id'=>$app_id
        ];

        $update = [
        ];
        if(!empty($name)){
            $update['app_name'] = $name;
        }
        if(!empty($note)){
            $update['note'] = $note;
        }
        if(count($update) == 0){
            return ResultHelper::success('操作成功');
        }
        $update['update_time'] = time();
        return (new ClientsLogic())->save($map,$update);
    }
}