<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2017-06-26
 * Time: 16:12
 */

namespace app\src\clients\action;


use app\src\base\action\BaseAction;
use app\src\base\helper\PageHelper;
use app\src\base\utils\CodeGenerateUtils;
use app\src\clients\helper\RandomHelper;
use app\src\clients\logic\ClientsLogic;

class ClientsQueryAction extends BaseAction
{
    public function query($uid,$name='',PageHelper $pageHelper){
        $map = [
            'uid'=>$uid
        ];

        return (new ClientsLogic())->query($map,$pageHelper->queryParam());
    }
}