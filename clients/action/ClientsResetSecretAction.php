<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 2017-06-26
 * Time: 16:12
 */

namespace app\src\clients\action;


use app\src\base\action\BaseAction;
use app\src\base\helper\PageHelper;
use app\src\base\utils\CodeGenerateUtils;
use app\src\clients\helper\RandomHelper;
use app\src\clients\logic\ClientsLogic;

class ClientsResetSecretAction extends BaseAction
{
    public function resetSecretByID($id){
        $map = [
            'id'=>$id
        ];
        $update = [
            'app_secret'=>RandomHelper::getAppSecret($id)
        ];
        $update['update_time'] = time();
        return (new ClientsLogic())->save($map,$update);
    }
    public function resetSecretByAppID($app_id){
        $map = [
            'app_id'=>$app_id
        ];

        $update = [
            'app_secret'=>RandomHelper::getAppSecret(rand(100,999))
        ];
        $update['update_time'] = time();
        return (new ClientsLogic())->save($map,$update);
    }
}